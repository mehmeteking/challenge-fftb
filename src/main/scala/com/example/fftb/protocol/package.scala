package com.example.fftb

package object protocol {

  type Res[A] = Fut[A]
  type Per = Person
  type Att = Attribute
  type Rch = RichestPerson

}
