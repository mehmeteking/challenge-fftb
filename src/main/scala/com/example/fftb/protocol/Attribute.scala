package com.example.fftb
package protocol

case class Attribute(personId: String, name: String) extends Named
object Attribute