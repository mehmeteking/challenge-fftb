package com.example.fftb

import play.api.libs.json.{Reads, Json}

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scalaj.http.{HttpRequest, Http}
import protocol._

class PeopleListClient extends PeopleList {
  import PeopleListClient._

  override def people = expect[Seq[Per]](request(peopleEndp))
  override def skills(ids: Seq[String]) = expect[Seq[Att]](request(skillsEndp, Some(ids)))
  override def interests(ids: Seq[String]): Res[Seq[Att]] = expect[Seq[Att]](request(interestsEndp, Some(ids)))
  override def richest: Res[Rch] = expect[Rch](request(richestEndp))

}
object PeopleListClient {
  type Req = HttpRequest

  val peopleEndp = "/people"
  val skillsEndp = "/skills"
  val interestsEndp = "/interests"
  val richestEndp = "/richest"

  val personIds = "personIds"

  val prefix = "http://"
  val host = "localhost"
  val port = 3000

  implicit val perRead: Reads[Per] = Json.reads[Per]
  implicit val perSeqRead: Reads[Seq[Per]] = Reads.seq(perRead)
  implicit val attRead: Reads[Att] = Json.reads[Att]
  implicit val attSeqRead: Reads[Seq[Att]] = Reads.seq(attRead)
  implicit val rchRead: Reads[Rch] = Json.reads[Rch]

  def request(endp: String, persons: Option[Seq[String]] = None): Req = {
    val req = Http(s"$prefix$host:$port$endp")
    if (persons.isDefined) req.param(personIds, persons.get.mkString(",")) else req
  }

  def expect[T : Reads](request: Req): Res[T] = Future(Json.parse(request.asString.body).as[T])

}
