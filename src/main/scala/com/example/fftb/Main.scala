package com.example.fftb

import scala.language.postfixOps

object Main extends App {
  import PeopleList._

  def printRec(rec: Record) = {
    def merge(atts: Atts) = atts.map(_.name).mkString(", ")
    println(s"${rec.name}${if (rec.richest) " (richest)" else ""}\n- organization: ${rec.org}")
    println(s"- skills: ${merge(rec.skills)}\n- interests: ${merge(rec.interests)}")
  }

  (new PeopleListClient).fetchAll.values.foreach(printRec)

}
