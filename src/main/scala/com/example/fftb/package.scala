package com.example

package object fftb {

  type Atts = Seq[Named]
  type Fut[+T] = scala.concurrent.Future[T]
  type Rec = Record
  type Recs = Map[String, Record]

  trait Named { def name: String }
  object Named

  case class Record(name: String, org: String, skills: Atts, interests: Atts, richest: Boolean)
  object Record {
    def apply(name: String, org: String): Rec = Record(name, org, Seq.empty, Seq.empty, richest = false)
  }
}
