package com.example.fftb

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.language.postfixOps
import protocol._

trait PeopleList {

  def people: Res[Seq[Per]]
  def skills(id: String): Res[Seq[Att]] = skills(id :: Nil)
  def skills(ids: Seq[String]): Res[Seq[Att]]
  def interests(id: String): Res[Seq[Att]] = interests(id :: Nil)
  def interests(ids: Seq[String]): Res[Seq[Att]]
  def richest: Res[Rch]

}
object PeopleList {

  implicit class Fetcher(val repo: PeopleList) extends AnyVal {
    import Fetcher._

    def fetchRecords = repo.people.map(_.map(p => (p.id, Record(p.name, p.org))).toMap).resolve
    def populateSkills(rec: RecTpl) = (rec._1, rec._2.withSkills(repo.skills(rec._1).resolve))
    def populateInterests(rec: RecTpl) = (rec._1, rec._2.withInterests(repo.interests(rec._1).resolve))
    def fetchRichest = repo.richest.resolve.richestPerson.toString
    def fetchAll = fetchRecords.map(populateSkills).map(populateInterests).setRichest(fetchRichest)

  }
  object Fetcher {

    type RecTpl = (String, Record)
    val timeout = 5000 milliseconds

    implicit class Resolver[T](val future: Fut[T]) extends AnyVal {
      def resolve = Await.result(future, timeout)
    }
    object Resolver

    implicit class RecordBuilder(val person: Record) extends AnyVal {
      def withSkills(skills: Atts) = person.copy(skills = person.skills ++ skills)
      def withInterests(interests: Atts) = person.copy(interests = person.interests ++ interests)
    }

    implicit class Decorator(val recs: Recs) extends AnyVal {
      def setRichest(key: String) = recs.updated(key, recs(key).copy(richest = true))
    }
    object Decorator

  }
}
