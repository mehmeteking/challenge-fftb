package com.example.fftb

import org.specs2.mutable.Specification
import TestUtil._

class TestPeopleList extends Specification {


  "Fetcher" should {
    val pplList = mockPeopleList()

    "fetch records" in {

      "size of collection" should {
        val recs = pplList.fetchRecords
        "be same as list" in(recs.size must beEqualTo(defPersons.size))
      }

      "collection" should {
        val recs = pplList.fetchRecords
        "contain all persons" in {
          examplesBlock {
            for (i <- 0 to defPersons.size - 1) {
              s"person $i" should {
                "have correct name" in (recs(i.toString).name must beEqualTo(defPersons(i)._1))
                "have correct org" in (recs(i.toString).org must beEqualTo(defPersons(i)._2))
              }
            }
          }
        }
      }
    }

    "populate skills" in {
      examplesBlock {
        for (i <- 0 to defPersons.size - 1) {
          val rec = pplList.populateSkills(i.toString, Record("", ""))
          s"person $i" should {
            "have correct key" in (rec._1 must beEqualTo(i.toString))
            "have correct skills" in (rec._2.skills.map(_.name).toSet must beEqualTo(defSkills(i).toSet))
          }
        }
      }
    }

    "populate interests" in {
      examplesBlock {
        for (i <- 0 to defPersons.size - 1) {
          val rec = pplList.populateInterests(i.toString, Record("", ""))
          s"person $i" should {
            "have correct key" in (rec._1 must beEqualTo(i.toString))
            "have correct interests" in (rec._2.interests.map(_.name).toSet must beEqualTo(defInterests(i).toSet))
          }
        }
      }
    }

    "fetch richest" in {
      pplList.fetchRichest must beEqualTo(defRichest.toString)
    }
  }
}
