package com.example.fftb

import org.specs2.mock.Mockito
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import protocol._

object TestUtil extends Mockito {

  type PerArg = Seq[(String, String)]
  type AttArg = Seq[Seq[String]]
  type AttExt = ((Int, AttArg)) => Fut[Seq[Att]]

  val toAttrs: AttExt = t => Future(t._2(t._1).map(name => mockAttribute(t._1.toString, name)))
  val defRichest = 0
  val (p1, p2, p3, p4) = (("p1", "o1"), ("p2", "o2"), ("p3", "o3"), ("p4", "o4"))
  val defPersons = p1 :: p2 :: p3 :: p4 :: Nil
  val (s1, s2, s3) = ("s1", "s2", "s3")
  val defSkills = (s1 :: s2 :: s3 :: Nil) :: (s1 :: s2 :: Nil) :: (s1 :: Nil) :: Nil :: Nil
  val (i1, i2, i3) = ("i1", "i2", "i3")
  val defInterests = (i1 :: i2 :: i3 :: Nil) :: (i1 :: i2 :: Nil) :: (i1 :: Nil) :: Nil :: Nil

  def mockPeopleList(persons: PerArg = defPersons, skills: AttArg = defSkills, interests: AttArg = defInterests,
                     richest: Int = defRichest) = {
    val ppl = mock[PeopleList]
    ppl.people returns Future(persons.zipWithIndex.map(t => mockPerson(t._2.toString, t._1._1, t._1._2)))
    ppl.skills(anyString) answers { id => toAttrs(id.toString.toInt, skills) }
    ppl.interests(anyString) answers { id => toAttrs(id.toString.toInt, interests) }
    ppl.richest returns Future(mockRichest(richest))
    ppl
  }

  def mockRecord(name: String, org: String) = {
    val rec = mock[Record]
    rec.name returns name
    rec.org returns org
    rec
  }

  def mockPerson(id: String, name: String, org: String) = {
    val per = mock[Person]
    per.id returns id
    per.name returns name
    per.org returns org
    per
  }

  def mockAttribute(id: String, name: String) = {
    val att = mock[Attribute]
    att.personId returns id
    att.name returns name
    att
  }

  def mockRichest(id: Int) = {
    val rch = mock[RichestPerson]
    rch.richestPerson returns id
    rch
  }

}
