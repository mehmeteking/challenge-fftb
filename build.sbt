name := "fftb"

scalaVersion := "2.11.5"

resolvers += "Typesafe Repo" at "http://repo.typesafe.com/typesafe/releases/"

libraryDependencies ++= Seq(
  "org.scalaj"        %% "scalaj-http" % "1.1.4",
  "com.typesafe.play" %% "play-json"   % "2.3.4",
  "org.specs2"        %% "specs2-core" % "2.4.16" % Test,
  "org.specs2"        %% "specs2-mock" % "2.4.16" % Test
)
